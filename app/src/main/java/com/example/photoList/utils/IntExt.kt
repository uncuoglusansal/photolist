package com.example.photoList.utils

fun Int?.toIntOrZero() = this ?: 0