package com.example.photoList.data

import com.example.photoList.model.ImageResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface PixabayApi {

    @Headers("Cache-Control: max-age=86400")
    @GET("?key=$KEY")
    suspend fun searchImages(
            @Query("q") query: String,
            @Query("page") page: Int,
            @Query("per_page") perPage: Int
    ): ImageResponse

    companion object {
        const val KEY = "39676995-61d7fe644b792423357d7928e"
    }
}