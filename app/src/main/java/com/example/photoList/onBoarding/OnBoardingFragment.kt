package com.example.photoList.onBoarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.photoList.R
import com.example.photoList.databinding.FragmentOnBoardingBinding

class OnBoardingFragment : Fragment() {

    private var viewBinding: FragmentOnBoardingBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentOnBoardingBinding.inflate(layoutInflater)
        return viewBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding?.apply {

            loginBtn.setOnClickListener {
                val currentDestination = findNavController().currentDestination
                if (currentDestination?.id != R.id.loginFragment) {
                    findNavController().navigate(R.id.action_onBoardingFragment_to_loginFragment)
                }

            }
            registerBtn.setOnClickListener {
                val currentDestination = findNavController().currentDestination
                if (currentDestination?.id != R.id.registerFragment) {
                    findNavController().navigate(R.id.action_onBoardingFragment_to_registerFragment)
                }
            }


        }

    }
}