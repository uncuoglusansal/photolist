package com.example.photoList.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.photoList.R
import com.example.photoList.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private var viewBinding: FragmentLoginBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentLoginBinding.inflate(layoutInflater)
        return viewBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding?.apply {


            createBtn.setOnClickListener {
                val currentDestination = findNavController().currentDestination
                if (currentDestination?.id != R.id.registerFragment) {
                    findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
                }
            }

            signInBtn.setOnClickListener {
                val currentDestination = findNavController().currentDestination
                if (currentDestination?.id != R.id.imageListFragment) {
                    findNavController().navigate(R.id.action_loginFragment_to_imageListFragment)
                }
            }

        }

    }
}