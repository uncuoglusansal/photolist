package com.example.photoList.register

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.photoList.R
import com.example.photoList.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {

    private var viewBinding: FragmentRegisterBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewBinding = FragmentRegisterBinding.inflate(layoutInflater)
        return viewBinding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewBinding?.apply {

            alreadyBtn.setOnClickListener {
                val currentDestination = findNavController().currentDestination
                if (currentDestination?.id != R.id.loginFragment) {
                    findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
                }
            }

        }

    }
}